
Imports System
Imports System.IO
Imports System.Linq
'Imports System.Data.SqlClient
Imports System.Threading.Tasks

Imports System.Collections.Generic
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Text
Imports System.Windows.Forms

Imports Application = System.Windows.Forms.Application
'Imports UtiliSave.Common
Imports UtiliSave.DataLayer.SqlHelper
Imports UtiliSave.ExchangeServerHandler
Imports UtiliSave.ExchangeServerHandler.ExchangeServer2013Object
Imports UtiliSave.IntervalDataParser
'Imports UtiliSave.ExchangeServerHandler.TrackExchServerActions

Imports Microsoft.Exchange.WebServices.Data
Imports UtiliSave.DataLayer
Imports MySql.Data.MySqlClient
Imports System.Data.SqlClient

Public Class IntervalDataProcessing

	Private WithEvents tmTracker As Timer

	Private clsExchWebServActions As TrackExchServer2013Actions
	Private clsIDRParser As IntervalDataParser.Parser

#Region "Working Variables"

	Private strReturn As String
	Private shouldContinue As Boolean
	Private timerCounter As Integer
	Private processError As Boolean

	Private eventExchangeID As String
	Private accountIDsList As Hashtable	' List(Of Integer)

	Private outputDirectory As String = My.Settings.OutputDirectory
	Private parsedDirectory As String = My.Settings.ParsedDirectory
	Private failedDirectory As String = My.Settings.FailedDirectory

	Private initProcess As Boolean
	Private isStarted As Boolean = False

#End Region	 ' W O R K I N G    V A R I A B L E S


#Region " ** Private Procedures ** "


	Private Sub Form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

		Try
			getAppSettingInfo()

			lblStatusInfo.Text = "Click START button to begin" & vbCrLf & "track exchange server IDR Emails."

			Application.DoEvents()

		Catch exp As Exception
			MessageBox.Show(exp.Message, "Interval Data Processing", MessageBoxButtons.OK, MessageBoxIcon.Error)
			End
		End Try

	End Sub



	Private Sub cmdStart_Click(sender As Object, e As EventArgs) Handles cmdStart.Click

		Try
			isStarted = Not isStarted

			If isStarted Then

				cmdStart.Text = "Stop"
				cmdStart.ForeColor = System.Drawing.Color.Firebrick

				shouldContinue = True

				If tmTracker Is Nothing Then

					tmTracker = New Timer
					m_ExchService = New ExchangeService

				End If

				lblStatusInfo.Text = "Checking for existing exchange server" & vbCrLf & " IDR Files or Activities..."

				Application.DoEvents()

				tmTracker.Enabled = True
				tmTracker.Interval = 2000
				''tmTracker.Start()

				initProcess = True

			Else
				cmdStart.Text = "Start"
				cmdStart.ForeColor = System.Drawing.Color.ForestGreen

				shouldContinue = False

				lblStatusInfo.Text = "Stopping..."
				Application.DoEvents()

				tmTracker.Interval = 2000

			End If

		Catch ex As Exception
			'ex.Message 
		End Try


	End Sub


	'Private Sub btnStart_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStart.Click

	'    btnStart.Enabled = False
	'    btnStop.Enabled = True

	'    shouldContinue = True

	'    tmTracker = New Timer

	'    lblStatusInfo.Text = "Checking for existed exchange server" & vbCrLf & "ACTIVITIES..."

	'    Application.DoEvents()

	'    tmTracker.Enabled = True
	'    tmTracker.Interval = 2000
	'    ''tmTracker.Start()
	'    m_ExchService = New ExchangeService
	'    initProcess = True

	'End Sub


	'Private Sub btnStop_Click(ByVal sender As Object, ByVal e As System.EventArgs)

	'    shouldContinue = False

	'    lblStatusInfo.Text = "Stopping..."
	'    Application.DoEvents()

	'    tmTracker.Interval = 2000

	'End Sub


	Private Sub LogMessage(ByVal message As String, Optional ByVal showDate As Boolean = True, Optional ByVal clearList As Boolean = False)

		If clearList OrElse listBox1.Items.Count > 100 Then
			listBox1.Items.Clear()
		End If

		If showDate Then
			message = "(" & Date.Now.ToString("MM/dd/yyyy HH:mm:ss") & ") " & message
		End If

		listBox1.Items.Add(message)
		listBox1.SelectedIndex = listBox1.Items.Count - 1

	End Sub


	Private Sub tmTracker_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles tmTracker.Tick

		Dim orderedFiles As Linq.IOrderedEnumerable(Of IO.FileInfo)
		Dim filesToParse() As String

		Dim isSuccess As Boolean

		Try
			If shouldContinue Then

				timerCounter += 1
				If timerCounter = 1 Then

					tmTracker.Enabled = False

					lblStatusInfo.Text = "Checking for existing files"
					LogMessage("", False, True)
					Application.DoEvents()

					orderedFiles = New DirectoryInfo(outputDirectory).GetFiles() _
											.OrderBy(Function(f As FileInfo) f.LastWriteTime)

					filesToParse = orderedFiles.Select(Function(file) file.FullName).ToArray()
					If Not filesToParse.Length = 0 Then

						LogMessage("Found " & filesToParse.Length.ToString() & " files.", False)
						LogMessage("", False)
						Application.DoEvents()

						isSuccess = ProcessExistingFiles(filesToParse)

						'isSuccess = MarkAccountsToProcess()
					End If

					LogMessage("Checking for coned emails", False, True)
					LogMessage("", False)

					Application.DoEvents()

					isSuccess = processConEdIntervalReports()

					If Not shouldContinue Then

						lblStatusInfo.Text = "Waiting for new Activities..."
						shouldContinue = True
					End If

					timerCounter = 0
					Application.DoEvents()

				End If
			Else

				If lblStatusInfo.Text = "Stopping..." Then
					lblStatusInfo.Text = "Process stopped by user." & vbCrLf & "Click START to resume."

				Else
					shouldContinue = True
				End If

				Application.DoEvents()

			End If

		Catch exp As Exception
			strReturn = exp.Message
		Finally
			If Not strReturn = "" Then
				timerCounter = 0
				lblStatusInfo.Text = "Error: " & strReturn
				strReturn = ""
				Application.DoEvents()
			End If

			If shouldContinue Then

				tmTracker.Interval = m_TimerInterval
				tmTracker.Enabled = True
			Else
				tmTracker.Enabled = False
			End If

			LogMessage("", False, True)
			Application.DoEvents()

		End Try

	End Sub


	Private Function processConEdIntervalReports() As Boolean

		Dim clsExchWebServerMessage As ExchangeServer2013Message

		Dim isSuccess As Boolean = True

		Try
			If initProcess Then

				clsExchWebServActions = New TrackExchServer2013Actions(initProcess, m_IntervalReportsMailBox)
				initProcess = False
			End If

			lblStatusInfo.Text = "Checking Coned report emails..."
			Application.DoEvents()

			clsExchWebServActions.trackExchangeServerActivities(ExchActionsType.Message, MessageSearchOption.WithAttachments, "Trending Report") ', m_IntervalReportsMailBox)
			If clsExchWebServActions.IsError Then

				strReturn = clsExchWebServActions.ErrorDesc

				shouldContinue = False
			Else

				clsExchWebServerMessage = clsExchWebServActions.ExchServerMessage

				If clsExchWebServerMessage.EWSEmailMessages IsNot Nothing Then

					If clsExchWebServerMessage.EWSEmailMessages.Count = 0 Then

						shouldContinue = False
					Else

						lblStatusInfo.Text = "Processing Coned report emails..."
						Application.DoEvents()

						For Each ccIDRMessage As EmailMessage In clsExchWebServerMessage.EWSEmailMessages

							If ccIDRMessage.HasAttachments Then

								lblStatusInfo.Text = "Loading Coned report attachment files..."
								Application.DoEvents()

								isSuccess = clsExchWebServerMessage.GetMessageAttacments(ccIDRMessage, outputDirectory, ".csv")
								If isSuccess Then

									'' DELETE MESSAGE ???
									clsExchWebServActions.deleteMoveExchangeServerMessage(ccIDRMessage)
								Else

									'strReturn = clsEWSMessage.ErrorDesc
									initProcess = True
								End If
							Else
								'' DELETE MESSAGE ???
								clsExchWebServActions.deleteMoveExchangeServerMessage(ccIDRMessage)
							End If

						Next

						Application.DoEvents()

						''If Not clsExchWebServerMessage.EWSEmailAttachments.Count = 0 Then

						''	isSuccess = ProcessAttachmentFiles(clsExchWebServerMessage.EWSEmailAttachments)

						''	'isSuccess = MarkAccountsToProcess()

						''End If

					End If
				Else

					shouldContinue = False
				End If
			End If

		Catch ex As Exception
			strReturn = ex.Message
			isSuccess = False
		End Try
		Return isSuccess

	End Function


	Private Function ProcessExistingFiles(ByVal FilesToParse() As String) As Boolean

		Dim isSuccess As Boolean = True

		Try
			lblStatusInfo.Text = "Processing Coned IDR reports files..."
			Application.DoEvents()

			For Each fileName As String In FilesToParse

				isSuccess = ParseFile(fileName)

			Next

		Catch ex As Exception
			strReturn = ex.Message
			isSuccess = False
		End Try
		Return isSuccess

	End Function


	''Private Function ProcessAttachmentFiles(ByVal emailAttachments As List(Of Attachment)) As Boolean

	''	Dim fileAttachment As FileAttachment

	''	Dim isSuccess As Boolean = True

	''	Try
	''		lblStatusInfo.Text = "Processing Coned IDR reports files..."
	''		Application.DoEvents()

	''		For Each emailAttachment As Attachment In emailAttachments

	''			fileAttachment = TryCast(emailAttachment, FileAttachment)
	''			''isSuccess = ParseFile(fileAttachment.FileName)
	''		Next

	''	Catch ex As Exception
	''		strReturn = ex.Message
	''		isSuccess = False
	''	End Try
	''	Return isSuccess

	''End Function


	Private Function ParseFile(ByVal fileToParse As String) As Boolean

		Dim fileInfo As Parser.FileInfo

		Dim fileDir As String
		Dim fileName As String
		Dim fileExt As String
		Dim index As Integer = 1

		Dim processedFilePath As String
		Dim IsSuccess As Boolean = True
		'Dim result As Boolean = False

		Try
			fileInfo = New Parser.FileInfo(fileToParse)

			clsIDRParser = New Parser(fileInfo)

			LogMessage("Parsing file: " & fileInfo.FilePath)

			Application.DoEvents()

			Parallel.Invoke(Sub() IsSuccess = clsIDRParser.Parse())
			'IsSuccess = clsIDRParser.Parse()
			If IsSuccess Then

				LogMessage("Success")

				MarkAccountsToProcess(clsIDRParser)

				processedFilePath = parsedDirectory
			Else
				LogMessage("Failed " & If(clsIDRParser.ErrorMessage = "", "", "- " & clsIDRParser.ErrorMessage))

				processedFilePath = failedDirectory
				'System.Diagnostics.Debug.Print("Failed " + MyFileInfo.FilePath + " " + DateTime.Now.ToString());
			End If

			LogMessage("", False)
			Application.DoEvents()

			processedFilePath &= fileInfo.FileName

			index = 1
			fileDir = Path.GetDirectoryName(processedFilePath)
			fileName = Path.GetFileNameWithoutExtension(processedFilePath)
			fileExt = Path.GetExtension(processedFilePath)

			If Not Directory.Exists(fileDir) Then
				Directory.CreateDirectory(fileDir)
			End If

			While File.Exists(processedFilePath)

				processedFilePath = fileDir & "\" & fileName & "(" & index & ")" & fileExt
				index += 1
			End While

			If fileInfo.Action = Parser.FileAction.Move Then

				File.Move(fileInfo.FilePath, processedFilePath)
			Else
				File.Copy(fileInfo.FilePath, processedFilePath)
			End If

			clsIDRParser = Nothing

		Catch ex As Exception
			IsSuccess = False
		End Try

		Return IsSuccess

	End Function


	Private Sub MarkAccountsToProcess(Optional accountInfo As Parser = Nothing)

		Dim strSQL As String
		Dim i As Integer

		Try
			strSQL = "INSERT INTO Egos_ProcessedAccounts " & ControlChars.CrLf &
									" ([PA_AccountID],[PA_AccountInvoicePrefix],[PA_AccountNumber],[PA_RetrieveDate],[PA_ComputerName]) " &
							" VALUES (@AccountID, 'E', '@AccountNumber', '" & Date.Now().ToString() & "', 'Default' "

			If accountInfo IsNot Nothing Then

				strSQL = strSQL.Replace("@AccountID", accountInfo.AccountID)
				strSQL = strSQL.Replace("@AccountNumber", accountInfo.AccountNumber)

				i = SqlHelper.ExecuteNonQuery(SqlHelper.strCN, CommandType.Text, strSQL)
			End If

		Catch ex As Exception
			'ex.Message 
		End Try

	End Sub

#End Region


End Class