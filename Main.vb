Imports Microsoft.Exchange.WebServices.Data

Imports UtiliSave.DataLayer.SqlHelper

Module Main

#Region " Public Variables "

    Public m_TimerInterval As Integer
    Public m_ExchService As ExchangeService

    'Public m_AttchmentDownloadToDir As String
    Public m_IntervalReportsMailBox As String

#End Region

#Region " ** E N U M S ** "

#End Region

    Public Sub getAppSettingInfo()

        Try
            m_TimerInterval = My.Settings.TimerInterval
            m_IntervalReportsMailBox = My.Settings.IntervalReportsMailBoxAddress
            'm_AttchmentDownloadToDir = My.Settings.OutputDirectory

            'gl_numberOfDelayCycles = My.Settings.NoOfDelayCycles

        Catch exp As Exception
            Throw New Exception(exp.Message)
        End Try

    End Sub


    'Friend Sub logError()

    '    Dim currLog As New EventLog("Application", Environment.MachineName, "ExchangeServerHandler")

    '    Application.DoEvents()
    '    currLog.WriteEntry(_errotext, EventLogEntryType.Warning)

    'End Sub


#Region " I/O "

    Dim strSQL As String

#End Region


End Module
